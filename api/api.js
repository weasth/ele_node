const express = require("express");
const router = express.Router();
const user = require("../controllers/userController");
const menus = require("../controllers/menuController");

router.post("/user/login",user.login)
router.post("/user/add",user.add)
router.get("/menus/get",menus.get)

module.exports = router;