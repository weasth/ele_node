const dbconfig = require("../util/dbconfig");

getIndex = (req,res) => {
    let sql = "select * from users";
    let sqlArr = [];
    let callback = (err,data) => {
        if (err) {
            console.log(err)
        }else {
            res.json({code:1,data})
        }
    }
    dbconfig.sqlConnect(sql,sqlArr,callback)
}

module.exports = {
    getIndex
}