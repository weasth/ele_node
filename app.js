const express = require("express");
const app = express();
const bodyParser = require("body-parser")

app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json())
//  允许跨域20200329
app.all("*",(req,res,next) => {
    //设置允许跨域的域名，*代表允许任意域名跨域
    res.header("Access-Control-Allow-Origin","*");
    //允许的header类型
    res.header("Access-Control-Allow-Headers","*");
    //跨域允许的请求方式 
    res.header("Access-Control-Allow-Methods","DELETE,PUT,POST,GET,OPTIONS");
    if (req.method.toLowerCase() == 'options')
        res.sendStatus(200);  //让options尝试请求快速结束
    else
        next();
})

const indexRouter = require("./routes/index")
const api = require("./api/api")
app.use("/",indexRouter)
app.use("/api",api)
// app.use("/api",api);
// app.use("/api")
app.listen(3000,(req,res) => {
    console.log("3000")
})